module.exports = {
    json: {
        limit: '5mb',
        strict: true,
        types: ['application/json', 'application/csp-report']
    },
    raw: {
        types: ['text/plain']
    },
    form: {
        types: ['application/x-www-form-urlencoded']
    },
    files: {
        types: ['multipart/form-data'],
        maxSize: '20mb',
        autoProcess: true,
        processManually: new Array
    }
};