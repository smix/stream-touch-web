const providers = [
    '@adonisjs/framework/providers/AppProvider',
    '@adonisjs/framework/providers/ViewProvider',
    '@adonisjs/bodyparser/providers/BodyParserProvider',
    '@adonisjs/cors/providers/CorsProvider'
];

module.exports = {
    providers: providers,
    aceProviders: new Array,
    aliases: {},
    commands: new Array
};