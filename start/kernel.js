const Server = use('Server');

const global = ['Adonis/Middleware/BodyParser'];
const server = ['Adonis/Middleware/Static', 'Adonis/Middleware/Cors'];

Server.registerGlobal(global).use(server);