const Route = use('Route');

Route.get('', function (node) {
    return node.response.status(200).send('Stream Touch');
}).as('home');

Route.group(function () {
    Route.get('', 'TwitchController.login');
    Route.post('app-get-token', 'TwitchController.tokenPost');
    Route.post('app-refresh-token', 'TwitchController.refreshPost');
}).prefix('twitch');

Route.group(function () {

}).prefix('app');