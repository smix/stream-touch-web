const format = use('util').format;
const qs = use('querystring');
const axios = use('axios');
const Env = use('Env');

class TwitchController {
    constructor() {
        this.redirect = format('%s/twitch/auth', Env.get('APP_URL'));
    }

    login(node) {
        const config = {
            client_id: Env.getOrFail('TWITCH_CLIENT_ID'),
            redirect_uri: this.redirect,
            response_type: 'code',
            scope: 'chat:read chat:edit channel:moderate user:read:email user:edit:broadcast'
        };
        const authorize = format('https://id.twitch.tv/oauth2/authorize?%s', qs.stringify(config));
        return node.response.redirect(authorize);
    }

    async tokenPost(node) {
        const data = node.request.all();
        const params = qs.stringify({
            client_id: Env.getOrFail('TWITCH_CLIENT_ID'),
            client_secret: Env.getOrFail('TWITCH_CLIENT_SECRET'),
            redirect_uri: this.redirect,
            code: data.code,
            grant_type: 'authorization_code'
        });
        const resp = await axios.post('https://id.twitch.tv/oauth2/token', params);
        delete resp.data.scope;
        return node.response.json(resp.data);
    }

    async refreshPost(node) {
        const data = node.request.all();
        const params = qs.stringify({
            client_id: Env.getOrFail('TWITCH_CLIENT_ID'),
            client_secret: Env.getOrFail('TWITCH_CLIENT_SECRET'),
            refresh_token: data.refresh_token,
            grant_type: 'refresh_token'
        });
        const resp = await axios.post('https://id.twitch.tv/oauth2/token', params);
        delete resp.data.scope;
        return node.response.json(resp.data);
    }
}

module.exports = TwitchController;